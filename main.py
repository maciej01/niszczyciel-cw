# -*- coding: utf-8 -*-

#.OOO..U...U..O.O.O..O.O.......OOOOO..OOOOO..OOOOO..O..O.....OOO
#.O....U...U...OOO...O.O.......O...O..O......O......O..O.....OOO
#.OOO..U...U..OOOOO..OO........OOOOO..OOOOO..O......O..O......O.
#.O....U...U...OOO...O.O.......O...O......O..O......O..O........
#.O....UUUUU..O.O.O..O..O......O...O..OOOOO..OOOOO..O..O......O.
from __future__ import division
import sys, os, time, random, math, socket, string, select, ftplib
import unicodedata
import os.path
import glob, time
import urllib, urllib2
import subprocess
import ctypes
import ConfigParser
from threading import Timer
from time import gmtime, strftime
global NICKNAME, IDENT, REALNAME, CHANNEL, NEWCATS
NICKNAME = '' #CatBot'
IDENT = '' #FeelsGood'
REALNAME = '' #JackieChan'
CHANNEL = u'' #catbot' #third cat -7 lines

NEWCATS = []
def utf8_convert(original):
    return ''.join(c for c in unicodedata.normalize('NFKD', original) if unicodedata.category(c) != 'Mn')
def parsemsg(s):
    prefix = ''
    trailing = []
    if s == "":
        return -1, -1, -1
    if s[0] == ':':
        prefix, s = s[1:].split(' ', 1)
    if s.find(' :') != -1:
        s, trailing = s.split(' :', 1)
        args = s.split()
        args.append(trailing)
    else:
        args = s.split()
    command = args.pop(0)
    return prefix, command, args

class Bot:
 
    def __init__(self, host='irc.esper.net', port=6667):
        global NICKNAME, IDENT, REALNAME, CHANNEL

        self.fetch_settings()

        NICKNAME = self.nickName
        IDENT = self.ident
        REALNAME = self.realName
        CHANNEL = self.channel

        self.owners = []
        self.active_channels = []
 
        self.receiveBuffer = ""
 
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socketDelay = 0.6
        self.socket.settimeout(self.socketDelay)
        self.joined = 0
        self.spamCounter = -1
        self.spamMessage = ""
        self.spamChannel = ""
        self.pokemonSpam = False

        self.sendQueue = []
        self.sendChannel = CHANNEL

        self.ftpConnection = -1
        self.fileObject = -1
        self.cdrom = 0
        self.cdromSet = 0

        self.fetch_owners()

        self.commands = {}
        self.fetch_plugins()

        self.idioci = []
        self.reset_idioci()
        self.logfile = open('log.txt', 'a')

    def reset_idioci(self):
        print "Reset!"
        self.idioci = []
        Timer(600, self.reset_idioci, (), {}).start()
        
    def fetch_settings(self):
        config = ConfigParser.ConfigParser()
        config.read('settings/config.ini')
        try:
            self.server = config.get('Server', 'Server')
            self.host = config.get('Server', 'Server')
            self.port = int(config.get('Server', 'Port'))
            self.channel = config.get('Server', 'Channel')

            self.nickName = config.get('Bot', 'Nick')
            self.ident = config.get('Bot', 'Ident')
            self.realName = config.get('Bot', 'RealName')
        except:
            print "[!] Error have happened while fetching settings from settings/config.ini!"
            sys.exit(1)

    def fetch_plugins(self):
        listdir_ret = os.listdir("plugins")
        if not listdir_ret:
            print "[!] No plugins found in plugins/. Is that intended?"
            return 0
        files = []
        for file_obj in listdir_ret:
            if '.py' in file_obj:
                files.append(file_obj)
        for file_obj in files:
            try:
                fb = open('plugins/'+file_obj, 'r')
            except:
                print "[!] Error with loading plugin "+file_obj+"!"
                return 0
            fc = fb.read()
            fb.close()
            try:
                exec(fc) ## IM SORRY
            except:
                print "[!] An error happened while executing plugin "+file_obj+"!"
                return 0
            filename = file_obj.split(".")[0]
            try:
                exec("self.commands[filename] = command_"+filename)
            except:
                print "[!] An error happened while adding plugin "+file_obj+" to dictionary!"
                return 0
            print "[*] Loaded "+file_obj+" succesfully!"
        print "[*] Loaded "+str(len(self.commands))+" commands succesfully!"

    def fetch_owners(self):
        owners_file = open('settings/owners.txt')
        for item in owners_file.read().split('\n'):
            self.owners.append(item+'!')
        print "[*] Found "+str(len(self.owners))+" people able to operate the bot."

    def connect(self):
        self.socket=socket.socket( )
        self.socket.connect((self.host, self.port))
        self.socket.send("NICK %s\r\n" % self.nickName)
        self.socket.send("USER %s %s cat :%s\r\n" % (self.ident, self.host, self.realName))
 
 
    def send(self, msg):
        self.socket.send('{0}\r\n'.format(msg))#.encode()
 
 
    def sendMsg(self, chan, msg):
        try:
            sendString = "PRIVMSG "+chan+" :"+msg
            self.send(sendString)
        except UnicodeDecodeError:
            #self.sendMsg(chan, "[!] Error sending string due to UnicodeDecodeError [most probably, the issue is polish characters]")
            utf8_converted = ""
            for character in msg:
                try:
                    utf8_converted += character.decode('ascii')
                except UnicodeDecodeError:
                    utf8_converted += '?'
            sendString = "PRIVMSG "+chan+" :"+utf8_converted
            self.send(sendString)
        except TypeError:
            sendString = "PRIVMSG "+chan+" :"+msg
            self.send(sendString)
        #print strftime("[*] [%H:%M:%S] [CatBot] to "+chan+": "+msg, gmtime())

    def sendMulti(self, chan, msg):
        splitLines = msg.split("\n")
        self.sendChannel = chan
        for item in splitLines:
            self.sendQueue.append(item)
 
    def receive(self):
        self.receiveBuffer = ""
        try:
            self.socket.settimeout(self.socketDelay)
            self.receiveBuffer = self.socket.recv(1024)
        except:
            self.receiveBuffer = ""
            return -1
        if 'PING' in self.receiveBuffer:
            print strftime("[*] [%H:%M:%S] Was pinged and responded to it!", gmtime())
        temp = string.split(self.receiveBuffer, "\n")
        for line in temp:
            try:
                line=string.rstrip(line)
                line=string.split(line)
               
                if(line[0] == "PING"):
                    self.socket.send("PONG %s\r\n" % line[1])
            except:
                pass
        return 1
 
    def handleInput(self):
        if self.sendQueue != []:
            self.sendMsg(self.sendChannel, self.sendQueue[0])
            self.sendQueue.pop(0)
        if self.spamCounter != -1:
            self.spamCounter -= 1
            if self.pokemonSpam == False:
                self.sendMsg(self.spamChannel, self.spamMessage)
            else:
                command_pokemon(self, ("useless poo "+self.spamChannel+" "+self.spamMessage).split(" ")) # i-i-i re-use code ;w;
            if self.spamCounter == 0:
                self.spamCounter = -1
        prefix, command, args = parsemsg(self.receiveBuffer)
        if prefix == -1:
            return -1
        if command == "JOIN":
            if prefix.split("!")[0] != self.nickName:
                self.sendMsg(CHANNEL, "\001ACTION wypierdalaj smieciu "+prefix.split("!")[0]+" z mojego kanalu!\001")
        if command == "PRIVMSG":
            channel = args[0]
            inputString = args[1]
            inputString = inputString[:-2]
            parsedString = inputString.split(" ")
            if channel != "#cw.pl":
                print strftime("[*] [%H:%M:%S] ["+prefix.split("!")[0]+"] to "+channel+": "+inputString, gmtime())
                self.logfile.write(strftime("[*] [%H:%M:%S] ["+prefix.split("!")[0]+"] to "+channel+": "+inputString, gmtime())+"\n")

            myCommand = False
            for item in self.owners:
                if prefix[:len(item)] == item:
                    myCommand = True
            if parsedString[0] == self.nickName and myCommand:# Commands
                try:
                    self.commands[parsedString[1]](self, parsedString)
                except KeyError:
                    self.sendMsg(CHANNEL, "[!] Error - command "+parsedString[1]+" not found!")
            else:
                joined = ' '.join(parsedString)
                nn = prefix.split("!")[0] # nick typa
                if not (nn in self.idioci):
                    odpowiedzi = ["Zamknij morde smieciu, sram ci na ryj.",
                    "Nikogo to nie obchodzi, mozesz juz sobie isc.",
                    "Odinstaluj, bo nawet boty cie ogrywaja.",
                    "Spierdalaj, idz sie do mamusi poplacz (:"]
                    self.sendMulti(nn, random.choice(odpowiedzi))
                    self.idioci.append(nn)


files = os.listdir("cats")#glob.glob("/cats/*.txt")
songs = os.listdir("songs")
for item in files:
    NEWCATS.append('cats/'+item)
for item in songs:
    NEWCATS.append('songs/'+item)

print "[*] Loaded "+str(len(NEWCATS))+" files ready to display. Use CatBot cat for that."


pyBot = Bot()
pyBot.connect()
pyBot.receive()

time.sleep(5)
pyBot.receive()
pyBot.timer = time.time()
counter = 20
while True:
    CantTouchThis = pyBot.receive()  
    if pyBot.joined == 0:
        if counter != 0:
            pyBot.socket.send("JOIN "+CHANNEL+" \r\n")
            counter -= 1
        else:
            pyBot.sendMsg(CHANNEL,'Siema smiecie <3')
            pyBot.joined = 1
    pyBot.handleInput()
pyBot.logfile.close()