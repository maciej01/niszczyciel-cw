def command_set(bot, inputArray):
    try:
        what = inputArray[2]
        withwhat = inputArray[3]
    except:
        bot.sendMsg(CHANNEL, "[!] Error: unsufficient parameters! (CatBot set <delay> <amount>")
        return 0
    if what == "delay":
        try:
            withwhat = withwhat.replace(",", ".")
            withwhat = float(withwhat)
        except:
            bot.sendMsg(CHANNEL, "[!] Error: socket delay must be float or integer! ")
            return 0
        bot.socketDelay = withwhat
    elif what == "owner":
        bot.owners.append(withwhat+'!')
        bot.sendMsg(CHANNEL, "Added "+withwhat+" as one of cat masters! :^)")